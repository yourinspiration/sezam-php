<?PHP

  require_once './sezam-conf.php';

  if (SEZAM_AUTO_AUTHENTICATION == true) {
    session_start();

    $sezam_obj = new sezam();

    if (SEZAM_AUTH_METHOD == 'BASIC') {
      if (!isset($_SERVER['PHP_AUTH_USER'])) {
        header('WWW-Authenticate: Basic realm="Sezam Realm"');
        header('HTTP/1.0 401 Unauthorized');
        die ("Not authorized");
      } else {
        $accessGranted = $sezam_obj->authenticateByUsername(
            $_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
        if ($accessGranted == true) {
            $_SESSION['SEZAM_USER'] = $_SERVER['PHP_AUTH_USER'];
        } else {
            header('WWW-Authenticate: Basic realm="Sezam Realm"');
            header('HTTP/1.0 401 Unauthorized');
            die ("Not authorized");
        }
      }
    }
  }

  class sezam {

    var $curl;
    var $url;

    function __construct() {
      $this->curl = curl_init();
      $this->url = 'https://sezam.yourinspiration.de/rest/accounts';

      curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($this->curl, CURLOPT_USERPWD, SEZAM_APPLICATION_ID . ":" . SEZAM_APPLICATION_KEY);
      // curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
      // curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0); 
    }

    public function authenticateByUsername($username, $password) {
      curl_setopt($this->curl, CURLOPT_URL, $this->url . "/username/" . $username);
      curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
      if (! $result = curl_exec($this->curl)) {
        trigger_error(curl_error($this->curl)); 
      }
      if ($result == null) {
        return false;
      } else {
        $user = json_decode($result ,true);
        if ($user['enabled'] == true) {
          if (SEZAM_APPLICATION_DIGEST == "MD5" && hash('md5', $password) == $user['password']) {
            return true;
          } else if (SEZAM_APPLICATION_DIGEST == "SHA-1" && hash('sha1', $password) == $user['password']) {
            return true;
          } else if (SEZAM_APPLICATION_DIGEST == "SHA-256" && hash('sha256', $password) == $user['password']) {
            return true;
          } else if (SEZAM_APPLICATION_DIGEST == "SHA-512" && hash('sha512', $password) == $user['password']) {
            return true;
          } else if (SEZAM_APPLICATION_DIGEST == "BCrypt" && password_verify($password, $user['password'])) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    }

  }

?>