<?PHP

// The application id.
define('SEZAM_APPLICATION_ID', "");
// The application key.
define('SEZAM_APPLICATION_KEY', "");
// The application digest.
define('SEZAM_APPLICATION_DIGEST', "");

// Set to true, if pages with sezam.php include should be
// secured by HTTP Basic Authentication
define('SEZAM_AUTO_AUTHENTICATION', true);

// The authentication method used by sesam.
// Only 'BASIC' is supported.
define('SEZAM_AUTH_METHOD', 'BASIC');

?>